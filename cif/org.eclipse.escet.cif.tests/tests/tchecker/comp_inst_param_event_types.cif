//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2023 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

group g:
  // Formal/actual mismatch: only one of them has a type.

  event e;
  event bool eb;

  group def P(event e):
  end

  group def Pb(event bool e):
  end

  p: P(eb);
  pb: Pb(e);

  // Formal/actual mismatch: both have type, but not equal types.

  event int ei;
  event real er;

  group def Pi(event int e):
  end

  group def Pr(event real e):
  end

  pi: Pi(er);
  pr: Pr(ei);

  // Formal/actual mismatch: both have type, but not equal types (int range).

  event int[2..2] ei2;

  group def Pi12(event int[1..2] e):
  end

  pi12: Pi12(ei2);

  // Formal/actual mismatch: void.
  event e_;
  event string es;
  event void ev;

  group def Ps(event string e): end
  group def Pv(event void e):   end
  group def P_(event e):        end

  ps1: Ps(e_);
  ps2: Ps(es);
  ps3: Ps(ev);

  pv1: Pv(e_);
  pv2: Pv(es);
  pv3: Pv(ev);

  p_1: P_(e_);
  p_2: P_(es);
  p_3: P_(ev);
end
