//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022, 2023 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

event e;

automaton a:
  disc int va, vb;
  disc list[3] int li;

  location l1:
    initial;
    edge e goto l2; // No update.
  location l2:
    edge e do va := 1, vb := 2 goto l3; // Multiple simple updates.
  location l3:
    edge e do (va, vb) := (1, 2) goto l4; // Multi-assignment.
  location l4:
    edge e do li[0] := 1 goto l5; // Partial assignment.
  location l5:
    edge e do if true: va := 1 end goto l1; // 'if' update.
end

automaton multi_asgn:
  disc int va, vb;

  location:
    initial;
    edge e do (va, vb) := (1, 2); // Multi-assignment.
end

automaton partial_asgn:
  disc list[3] int li;

  location:
    initial;
    edge e do li[0] := 1; // Partial assignment.
end

automaton if_upd:
  disc int v;

  location:
    initial;
    edge e do if true: v := 1 end; // 'if' update.
end

group g:
  automaton def A():
    disc int va, vb;
    disc list[3] int li;

    location l1:
      initial;
      edge e goto l2; // No update.
    location l2:
      edge e do va := 1, vb := 2 goto l3; // Multiple simple updates.
    location l3:
      edge e do (va, vb) := (1, 2) goto l4; // Multi-assignment.
    location l4:
      edge e do li[0] := 1 goto l5; // Partial assignment.
    location l5:
      edge e do if true: va := 1 end goto l1; // 'if' update.
  end
end

automaton all:
  disc int va, vb;
  disc list[3] int li;

  location:
    initial;
    edge e; // No update.

    edge e do va := 1, vb := 2; // Multiple simple updates.
    edge e do va := 3, vb := 4; // Multiple simple updates.

    edge e do (va, vb) := (1, 2); // Multi-assignment.
    edge e do (vb, va) := (3, 4); // Multi-assignment.

    edge e do li[0] := 1; // Partial assignment.
    edge e do li[1] := 2; // Partial assignment.

    edge e do if true: va := 1 end; // 'if' update.
    edge e do if true: va := 1 end; // 'if' update.
end
